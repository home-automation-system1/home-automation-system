import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import QRCode from 'example-plugin-qr-code';
import Weather from 'plugin-weather';
import Commands from 'plugin-local-commands';
import Spotify from 'plugin-spotify';
import axios from 'axios'

Vue.config.productionTip = false;

Vue.prototype.$plugins = []

const plugins = [QRCode, Weather, Commands, Spotify]

for (const plugin of plugins) {
  Vue.use(plugin, {
    router, plugins: Vue.prototype.$plugins
  })
}

let host = window.location.hostname;
let backend = 'http://' + host + ':8000'
const axiosInstance = axios.create({
  baseURL: backend,
  timeout: 5000,
  headers: {
    'Content-Type': 'application/json'
  }
});

Vue.prototype.$axios = axiosInstance

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
