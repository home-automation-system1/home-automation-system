// import { encode } from 'js-base64';
// import {sha256} from 'js-sha256';
//
const CLIENT_ID = 'f90f607554674593af4aa355b56f2b07'
// export const codeVerifier = 'bananasareawesome'

// async function sha256(plain) {
//     // returns promise ArrayBuffer
//     const encoder = new TextEncoder();
//     const data = await encoder.encode(plain);
//     return await window.crypto.subtle.digest('SHA-256', data);
// }
//
// function base64urlencode(a) {
//     // Convert the ArrayBuffer to string using Uint8 array.
//     // btoa takes chars from 0-255 and base64 encodes.
//     // Then convert the base64 encoded to base64url encoded.
//     // (replace + with -, replace / with _, trim trailing =)
//     return btoa(String.fromCharCode.apply(null, new Uint8Array(a)))
//         .replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '');
// }
//
// async function pkce_challenge_from_verifier(v) {
//     const hashed = await sha256(v);
//     return base64urlencode(hashed);
// }

class PkceChallenge {
    random(length, mask) {
        let result = "";
        let randomIndices = new Int8Array(length);
        window.crypto.getRandomValues(randomIndices);
        const byteLength = 256
        const maskLength = Math.min(mask.length, byteLength);
        const scalingFactor = byteLength / maskLength;

        for (var i = 0; i < length; i++) {
            result += mask[Math.floor(Math.abs(randomIndices[i]) / scalingFactor)];
        }
        return result;
    }

    base64UrlEncode(array) {
        return btoa(String.fromCharCode.apply(null, new Uint8Array(array)))
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/=+$/, '');
    }

    generateVerifier(length) {
        const mask = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~";
        return this.random(length, mask);
    }

    generateChallenge(length = 43) {
        this.verifier = this.generateVerifier(length);

        const encoder = new TextEncoder();
        const data = encoder.encode(this.verifier);
        return window.crypto.subtle.digest('SHA-256', data).then(array => { return { code_challenge: this.base64UrlEncode(array), code_verifier: this.verifier }; });
    }
}

export const generateCodeVerifier = async () => {
    return await (new PkceChallenge()).generateChallenge()
}


const authorisationRequest = async (initiator, challenge) => {
    return  {
        client_id: CLIENT_ID,
        response_type: "code",
        redirect_uri: window.location.origin + window.location.pathname,
        code_challenge_method: "S256",
        code_challenge: challenge,
        scope: ["streaming", "user-read-email", "user-read-private", "user-read-playback-state"].join(" ")
    }
}

export const doAuthorisationRequest = async (initiator, challenge) => {
    const baseEndpoint = 'https://accounts.spotify.com/authorize'
    const params = await authorisationRequest(initiator, challenge)
    const searchParams = new URLSearchParams(params);
    window.location.replace(baseEndpoint + '?' + searchParams.toString())
}

export const fetchAccessToken = async (initiator, code, code_verifier) => {
    const params = {
        client_id: CLIENT_ID,
        grant_type: 'authorization_code',
        code: code,
        redirect_uri: window.location.origin + window.location.pathname,
        code_verifier: code_verifier
    }
    const data = new URLSearchParams(params)
    // for (const key of Object.keys(params)) {
    //     data.append(key, params[key])
    // }
    
    return await initiator.$axios.post('https://accounts.spotify.com/api/token', data.toString(), {
        headers: {
            'content-type':'application/x-www-form-urlencoded'
        }
    })
}

export const refreshToken = async (initiator, refresh_token) => {
    const params = {
        grant_type: 'refresh_token',
        refresh_token,
        client_id: CLIENT_ID
    }
    const data = new URLSearchParams(params)
    // const data = new FormData()
    // for (const key of Object.keys(params)) {
    //     data.append(key, params[key])
    // }
    return await initiator.$axios.post('https://accounts.spotify.com/api/token', data, {
        headers: {
            'content-type':'application/x-www-form-urlencoded'
        }
    })
}