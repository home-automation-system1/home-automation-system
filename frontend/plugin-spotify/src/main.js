import Spotify from './views/Spotify'
import IMG from "./assets/plugin-image.jpg";

export default {
  install(Vue, options) {
    Vue.component('Spotify', Spotify)

    options.router.addRoutes([
      {
        path: '/spotify',
        name: 'Spotify',
        component: Spotify
      }
    ])

    options.plugins.push({
      name: 'Spotify Web SDK',
      image: IMG,
      path: '/spotify'
    })
  }
}
