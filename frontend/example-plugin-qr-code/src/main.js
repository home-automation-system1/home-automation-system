import QRCode from './views/QRCode'
import VueRouter from "vue-router";
import IMG from './assets/plugin-image.jpg'

export default {
  install(Vue, options) {
    Vue.use(VueRouter);
    Vue.component('QRCode', QRCode)

    options.router.addRoutes([
        {
            path: '/qr',
            name: 'QR code',
            component: QRCode
        }
    ])

    options.plugins.push({
      name: 'QR Code',
      image: IMG,
      path: '/qr'
    })
  }
}
