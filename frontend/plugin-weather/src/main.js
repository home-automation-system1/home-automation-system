import Weather from "./views/Weather";
import IMG from "./assets/plugin-image.jpg";

export default {
  install(Vue, options) {
    Vue.component('Weather', Weather)

    options.router.addRoutes([
      {
        path: '/weather',
        name: 'Weather',
        component: Weather
      }
    ])

    options.plugins.push({
      name: 'Weather View',
      image: IMG,
      path: '/weather'
    })
  }
}