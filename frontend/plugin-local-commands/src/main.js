import Commands from './views/Commands'
import IMG from "./assets/plugin-image.jpg";

export default {
  install(Vue, options) {
    Vue.component('Commands', Commands)

    options.router.addRoutes([
      {
        path: '/local-commands',
        name: 'Commands',
        component: Commands
      }
    ])

    options.plugins.push({
      name: 'Local Commands',
      image: IMG,
      path: '/local-commands'
    })
  }
}
