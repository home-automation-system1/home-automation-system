cd ./frontend
yarn install
yarn workspace core serve --port 80 &
cd ../backend
pip install uvicorn
python3 ./setup.py