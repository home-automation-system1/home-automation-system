import socket

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip = s.getsockname()[0]
    s.close()
    return ip

origins = ["http://localhost", "http://localhost:8080", get_ip() + ':8080']

print('Using origins', origins)

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_credentials=True,
    allow_origins=['*'],
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def root():
    return {"message": "Welcome to the home automation system. This is the backend and can be extended with plugins."}

