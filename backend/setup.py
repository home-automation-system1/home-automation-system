import glob, os
import subprocess

BLACKLISTED_MODULES = ['core', '__pycache__', '.gitignore', '.idea']


def install_requirements():
    path = os.path.dirname(os.path.abspath(__file__))

    os.chdir(path)
    for file in glob.glob("./**/requirements.txt"):
        print('Installing requirements for ', file)
        subprocess.run(["pip3", "install", "-q", "-r", file])

def link_routers(app):
    for module in os.listdir(os.path.dirname(__file__)):
        if module[-3:] != '.py' and module not in BLACKLISTED_MODULES:
            print('Found package @ ', module)
            package = __import__(module, locals(), globals())
            print(package)
            print('included router from package with name', package.name)
            app.include_router(
                package.router,
                prefix='/' + package.name,
                responses={404: {"description": "Not found"}},
            )
    del module

def start_backend(app):
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)

if __name__ == '__main__':
    install_requirements()

    from core.main import app
    link_routers(app)
    start_backend(app)

