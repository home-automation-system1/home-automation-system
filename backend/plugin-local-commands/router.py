import os
from fastapi import APIRouter

name = 'local-commands'
router = APIRouter()

@router.get("/shutdown")
async def shutdown():
    os.system('sudo shutdown -s')

@router.get("/screen-off")
async def turn_off_screen():
    os.system('echo 1 > /sys/class/backlight/rpi_backlight/bl_power')

@router.get("/screen-on")
async def turn_on_screen():
    os.system('echo 0 > /sys/class/backlight/rpi_backlight/bl_power')

@router.get("/set-brightness")
async def set_brightness(brightness:int):
    # Some value between 0 and 255
    os.system(f'sudo bash -c "echo {brightness} > /sys/class/backlight/rpi_backlight/brightness"')
