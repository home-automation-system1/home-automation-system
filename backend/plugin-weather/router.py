import requests
from fastapi import APIRouter

name = 'weather'
router = APIRouter()

API_KEY = '220ae06e94e92b1bb474428b61551d02'

@router.get("/all")
async def current_weather():
    weather = requests.get(
        f'https://api.openweathermap.org/data/2.5/onecall'
        f'?lat={51.0543422}&lon={3.7174243}'
        f'&units=metric'
        f'&appid={API_KEY}'
    )
    return weather.json()