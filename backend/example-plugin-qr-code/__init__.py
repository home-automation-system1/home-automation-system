from .router import router, name
# A plugin must export the following things:
# - A router
# - A prefix/name of the plugin
__all__ = ['router', 'name']