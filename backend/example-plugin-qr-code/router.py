import socket
from fastapi import APIRouter
from fastapi.responses import StreamingResponse
import qrcode
from io import BytesIO

name = 'qr'
router = APIRouter()


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip = s.getsockname()[0]
    s.close()
    return ip


def uri_encoded_password():
    return "%22bananas%20are%20cool%20things%2C%20are%20they%20not%3F%22"


def get_ip_with_password():
    return get_ip() + '?password=' + uri_encoded_password()


@router.get("/ip")
async def get_ip_address():
    return {
        'ip': get_ip()
    }


@router.get("/qr-code")
def get_qr_code():
    stream = BytesIO()
    image = qrcode.make(get_ip_with_password())
    image.save(stream)
    stream.seek(0)
    return StreamingResponse(stream)
